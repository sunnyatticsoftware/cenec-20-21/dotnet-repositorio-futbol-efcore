﻿using Microsoft.EntityFrameworkCore;

namespace Soccer.Infra.Repository.EntityFramework
{
    public class SoccerContext
        : DbContext
    {
        public SoccerContext(DbContextOptions<SoccerContext> options) 
            : base(options)
        {
        }
        
        // Añade DbSet por cada tabla que consideres oportuna.
        // Ejemplo:
        // public DbSet<GameEntity> Games { get; set; }
        // public DbSet<TeamEntity> Teams { get; set; }
        // etc.

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Utiliza este método solamente si lo necesitas
        }
    }
}